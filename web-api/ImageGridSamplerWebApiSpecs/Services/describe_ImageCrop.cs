﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Net.Mime;
using System.Text;
using FluentAssertions;
using ImageGridSamplerWebApi.Services;
using NSpec;
using SkiaSharp;
using Autofac;
using ImageGridSamplerWebApi.Models;

namespace ImageGridSamplerWebApiSpecs.Services
{
	/// <summary>
	/// Tests the Image Crop operations.
	/// </summary>
	class describe_ImageCrop : SpecBase
	{
		private string _sourceImagePath;
		private string _triangleImageProducedPath;
		private string _triangleClipImagePath;
		private string _triangleClipImageCellHighlightPath;
		private IImageCrop _imageCrop;

		/// <summary>
		/// NOTE: NSpec bug - ALL spec contexts are executed BEFORE class level before_all,
		/// however within each context class level before_all execution rules are correct
		/// as is the before_all execution order within the class hierarchy.
		/// </summary>
		void before_all()
		{			
			Trace.WriteLine(this.GetType().Name + ": before_all");
			_sourceImagePath = AssemblyDirectory + @"\Images\Source\blank-triangle-grid-60sq.jpg";
			_triangleImageProducedPath =
				@"..\..\..\..\..\ImageGridSamplerWebApi\bin\Content\Images\Processed\TriangleCrops\";
			_triangleClipImagePath = AssemblyDirectory + @"\Images\Expected\";
			_triangleClipImageCellHighlightPath = AssemblyDirectory + @"\Images\Expected\";

			// Delete files produced from last test run before proceeding.
			DeleteAllFilesFromDirectory(_triangleImageProducedPath);

			InitIoC();
			_imageCrop = IoCScope.Resolve<IImageCrop>();

		}

		void triangle_crop()
		{
			System.Diagnostics.Debugger.Launch();
			Trace.WriteLine(this.GetType().Name + ": triangle_crop");
			IList<SKBitmap> expectedTriangleImage = new List<SKBitmap>();
			IList<SKBitmap> expectedTriangleCellHighlightImage = new List<SKBitmap>();

			IList<SKBitmap> producedTriangleImage = new List<SKBitmap>();
			IList<SKBitmap> producedTriangleCellHighlightImage = new List<SKBitmap>();


			context[@"\Images\Source\blank-triangle-grid-60sq.jpg"] = () =>
			{
				context["Coordinate row-col crop"] = () =>
				{

					before = () => { DoImageGridLoop((row, col, imageIndex) =>
						{

							expectedTriangleImage.Add(GetSkiaBitmap(_triangleClipImagePath + row + col + "triangle.jpg"));
							expectedTriangleCellHighlightImage.Add(GetSkiaBitmap(_triangleClipImagePath + row + col + "triangleCellHighlight.jpg"));
							
							// Do the crop!
							IImageGridClipFilePaths imageGridClipFilePaths = _imageCrop.DoTriangleCrop(_sourceImagePath, row, col);

							producedTriangleImage.Add(GetSkiaBitmap(imageGridClipFilePaths.ClippedImagePath));
							producedTriangleCellHighlightImage.Add(GetSkiaBitmap(imageGridClipFilePaths.ClippedImageCellHighlightPath));


							// TEST VALIDATION -------------------------------------------------------------------------------------------------------------------------.
							//producedTriangleImage.Add(GetSkiaBitmap(_triangleClipImagePath + row + col + "triangle.jpg"));	// NOTE: uncomment to prove passing test.
							//producedTriangleImage.Add(GetSkiaBitmap(_sourceImagePath));										// NOTE: uncomment to prove failing test.

							//producedTriangleCellHighlightImage
							//	.Add(GetSkiaBitmap(_triangleClipImagePath + row + col + "triangleCellHighlight.jpg"));			// NOTE: uncomment to prove passing test.
							//producedTriangleCellHighlightImage.Add(GetSkiaBitmap(_sourceImagePath));							// NOTE: uncomment to prove failing test.

						});
					};

					context["Check cropped images for every grid cell."] = () =>
					{
						// Loops are supported in NSPec, but data driven tests are not.
						// Ref: https://github.com/nspec/NSpec/issues/64

						// System.Diagnostics.Debugger.Launch();
						// Compare pixel data for SKBitmap.Byte arrays.
						DoImageGridLoop((row, col, imageIndex) =>
						{
							it[@"-- Should match all pixel data for reference [ " + row + col + @" ] triangle image crop: \Images\Expected\" + row + col + "triangle.jpg"] = () =>
								{																	
									producedTriangleImage[imageIndex].Bytes.Should()
										.Equal(expectedTriangleImage[imageIndex].Bytes);
								};

							it[@"-- Should match all pixel data for reference [ " + row + col + @" ] triangle image crop grid cell highlight: \Images\Expected\" + row + col + "triangleCellHighlight.jpg"] = () =>
								{
									producedTriangleCellHighlightImage[imageIndex].Bytes.Should()
										.Equal(expectedTriangleCellHighlightImage[imageIndex].Bytes);
								};
						});

					};
				};
			};
		}

		#region Helper Methods

		private void DoImageGridLoop(Action<string, string, int> action)
		{
			int imageIndex = 0;
			// Loops are supported in NSPec, but data driven tests are not.
			// Ref: https://github.com/nspec/NSpec/issues/64
			// Loops not behaving as expected and cannot find examples - broken links.
			for (uint a = 65; a < 71; a++) // ASCII Char codes for A - F.
			{
				var row = ((char)a).ToString();

				for (int i = 1; i < 13; i++)
				{
					var col = i.ToString();

					Trace.WriteLine(
						this.GetType().Name + ": context: " +
						@"\Images\Source\blank-triangle-grid-60sq.jpg");
					Trace.WriteLine(this.GetType().Name + ": context: " + "Coordinate F2 crop");
					
					action(row, col, imageIndex++);

				}
			}

		}

		/// <summary>
		/// Get SkiaBitmap for image file path.
		/// </summary>
		/// <param name="imagePath"></param>
		/// <returns></returns>
		/// <remarks>
		/// Ref: http://visualcsharpdev.blogspot.com/2017/04/net-core-image-processing.html
		/// https://docs.microsoft.com/en-us/dotnet/api/skiasharp.skmanagedstream?view=skiasharp-1.60.3
		/// </remarks>
		private SKBitmap GetSkiaBitmap(string imagePath)
		{
			using (var input = File.OpenRead(imagePath))
			{
				using (var inputStream = new SKManagedStream(input))
				{

					SKBitmap bitmap = SKBitmap.Decode(inputStream);
					inputStream.Dispose();
					
					return bitmap;

				}
			}
		}

		#endregion

	}
}
