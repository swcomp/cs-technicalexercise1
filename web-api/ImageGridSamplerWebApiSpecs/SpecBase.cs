﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Autofac;
using ImageGridSamplerWebApi.IoC;
using ImageGridSamplerWebApi.Services;
using NSpec;

namespace ImageGridSamplerWebApiSpecs
{
	/// <summary>
	/// Base spec with useful methods for all specs.
	/// </summary>
	/// <remarks>
	/// NOTE: NSpecRunner is located here: cs-technicalexerise1\web-api\packages\NSpec.3.1.0\tools\net451\win7-x64
	/// </remarks>
	public abstract class SpecBase : nspec
	{

		/// <summary>
		/// NOTE: NSpec bug - ALL spec contexts are executed BEFORE class level before_all,
		/// however within each context class level before_all execution rules are correct
		/// as is the before_all execution order within the class hierarchy.
		/// </summary>
		void before_all()
		{
			Trace.WriteLine(this.GetType().Name + ": SpecBase : before_all");
		
		}

		protected void InitIoC()
		{

			var containerConfig = new ContainerConfig(){IsUnitTest = true};

			ImageGridSamplerWebApi.IoC.Container.Build(out _container, containerConfig);

			IoCScope = _container.BeginLifetimeScope();

		}

		private IContainer _container;
		protected IContainer Container
		{
			get { return _container; }
		}

		protected ILifetimeScope IoCScope { get; private set;}

		/// <summary>
		/// Gets directory of assembly code is in.
		/// </summary>
		/// <remarks>
		/// Ref: https://stackoverflow.com/a/283917
		/// </remarks>
		public static string AssemblyDirectory {
			get {
				string codeBase = Assembly.GetExecutingAssembly().CodeBase;
				UriBuilder uri = new UriBuilder(codeBase);
				string path = Uri.UnescapeDataString(uri.Path);
				return Path.GetDirectoryName(path);
			}
		}

		/// <summary>
		/// Deletes all files from directory
		/// </summary>
		/// <param name="directoryPath"></param>
		/// Ref: https://stackoverflow.com/a/1288747
		public static void DeleteAllFilesFromDirectory(string directoryPath)
		{
			System.IO.DirectoryInfo di = new DirectoryInfo(directoryPath);

			foreach (FileInfo file in di.GetFiles())
			{
				file.Delete();
			}
			foreach (DirectoryInfo dir in di.GetDirectories())
			{
				dir.Delete(true);
			}
		}

	}
}