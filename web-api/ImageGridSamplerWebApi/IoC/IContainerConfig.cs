﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImageGridSamplerWebApi.IoC
{
	public interface IContainerConfig
	{
		bool IsUnitTest { get; set; }
	}
}