﻿using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;

namespace ImageGridSamplerWebApi.IoC
{
	/// <summary>
	/// Configures and initialises Autofac container as required for application or unit tests.
	/// </summary>
    public class Container
    {

		private static IContainer _container;
		private static IContainerConfig _containerConfig;
		
	    /// <summary>
	    /// Builds container using configuration config.
	    /// </summary>
	    /// <param name="containerConfig"></param>
	    public static void Build(IContainerConfig containerConfig)
	    {
			_containerConfig = containerConfig;
		    Build();
	    }

		/// <summary>
		/// Builds container using configuration config and output reference param.
		/// </summary>
		/// <param name="autofacContainer"></param>
		/// <param name="containerConfig"></param>
		public static void Build(out IContainer autofacContainer, IContainerConfig containerConfig)
	    {
			_containerConfig = containerConfig;
		    Build(out autofacContainer);
	    }

		/// <summary>
		/// Builds container with output reference param.
		/// </summary>
		/// <param name="autofacContainer"></param>
	    public static void Build(out IContainer autofacContainer)
		{
			Build();
			autofacContainer = _container;
		}

	    public static void Build()
	    {
			// Create the container builder.
		    var builder = new ContainerBuilder();

		    // Init default ContainerConfig if none provided.
		    _containerConfig = _containerConfig ?? new ContainerConfig();

		    if (_containerConfig.IsUnitTest)
		    {
				// TODO: Refactor out this duplication.

			    // Register Autofac modules.
			    RegisterAutofacModules(builder);

			    // Set the dependency resolver to be Autofac.
			    _container = builder.Build();
			}
		    else
		    {
			    //// Get your HttpConfiguration.
			    //var config = GlobalConfiguration.Configuration;

			    //// Register your Web API controllers.
			    //builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

			    //// OPTIONAL: Register the Autofac filter provider.
			    //builder.RegisterWebApiFilterProvider(config);

			    //// OPTIONAL: Register the Autofac model binder provider.
			    //builder.RegisterWebApiModelBinderProvider();

			    //// Register Autofac modules.
			    //RegisterAutofacModules(builder);

			    //// Set the dependency resolver to be Autofac.
			    //_container = builder.Build();
			    //config.DependencyResolver = new AutofacWebApiDependencyResolver(_container);
			}			

		}

	    private static void RegisterAutofacModules(ContainerBuilder builder)
	    {

			// Load Autofac IoC registration modules.
		    builder.RegisterModule(new MainAutofacModule(_containerConfig));
		}

    }
}
