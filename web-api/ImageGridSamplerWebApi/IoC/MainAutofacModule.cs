﻿using Autofac;
using ImageGridSamplerWebApi.Models;
using ImageGridSamplerWebApi.Services;

namespace ImageGridSamplerWebApi.IoC
{
	public class MainAutofacModule : Module
	{
		private IContainerConfig _config;

		public MainAutofacModule(IContainerConfig config)
		{
			_config = config;
		}

		protected override void Load(ContainerBuilder builder) 
		{
			// registers main types [ Autofac 'components' ]

			// NOTE - IContainerConfig may have been passed in to alter default registrations.
			// IsUnitTest == true
			// if necessary alter/skip any registrations that would cause problems without WebAPI runtime.

			builder.RegisterType<ImageCrop>().As<IImageCrop>();

			builder.RegisterType<ImageGridClipFilePaths>().As<IImageGridClipFilePaths>();
		}

	}

}
 