﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImageGridSamplerWebApi.IoC
{
	/// <summary>
	/// Allows container to be configured for unit testing and other scenarios where default registrations may not work.
	/// </summary>
	public class ContainerConfig : IContainerConfig
	{
		public bool IsUnitTest { get; set;}
	}
}