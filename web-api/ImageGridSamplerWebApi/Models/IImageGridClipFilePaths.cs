﻿namespace ImageGridSamplerWebApi.Models
{
	public interface IImageGridClipFilePaths
	{
		string ClippedImagePath { get; set; }
		string ClippedImageCellHighlightPath { get; set; }
	}
}