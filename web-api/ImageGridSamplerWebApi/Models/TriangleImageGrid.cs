﻿using System;
using System.Web.Configuration;
using ImageGridSamplerWebApi.Services;
using SkiaSharp;

namespace ImageGridSamplerWebApi.Models
{
	public class TriangleImageGrid : ImageGrid
	{
		// TODO: Moq? this WebConfigurationManager calls in unit tests > register type in container and modify this constructor as no Web.config when 
		// execution from ImageGridSamplerWebApiSpecs
		public TriangleImageGrid(ImageSampler imageSampler) : 
			this(imageSampler, WebConfigurationManager.AppSettings["TriangleGridRowCount"], WebConfigurationManager.AppSettings["TriangleGridColCount"])
		{ 
		}

		public TriangleImageGrid(ImageSampler imageSampler, string rowCount, string colCount) : base(imageSampler)
		{
			var bRowCount = int.TryParse(rowCount, out _rowCount);
			var bColCount = int.TryParse(colCount, out _colCount);

			// TODO: Endpoint exception reporting.
			if (!bRowCount || !bColCount)
				throw new System.Exception(message: "Invalid rowCount and/or colCount parameter, integer required.");

		}

		#region properties

		private readonly int _rowCount;
		public override int RowCount
		{
			get => _rowCount;
			set => value = _rowCount;
		}

		private readonly int _colCount;
		public override int ColCount {
			get => _colCount;
			set => value = _colCount;
		}

		#endregion

		#region methods

		public override SKPath GetClipPath(string rowNum, string colNum, out SKRectI cellRect)
		{
			SKPath path = null;
			int row;
			int col;

			try {

				// TODO: support for multiple letter rows.
				row = GetRowNumberForLetters(rowNum);
				if (row < 1 || row > 26) throw new System.Exception(message: "Invalid rowNum parameter, A-Z letter required.");

				bool bCol = int.TryParse(colNum, out col);
				if (!bCol) throw new System.Exception(message: "Invalid colNum parameter, integer required.");

				// Workout path coordinates (V1x,V1y), (V2x, V2y), (V3x, V3y).
				SKImageInfo info = Sampler.SourceBitmap.Info;

				// Validate image size - Client image crop/resize required.
				if (info.Height != 60 || info.Width != 60 ) throw new System.Exception(message: "Invalid image size, 60x60 pixels required.");

				TriangleCoords coords = GetTriangleCoords(info, row, col, out cellRect);

				// Coords returned are:
				// Path coords to mask out the other triangle in the cell.

				// EVEN col num
				//coords.V1 = rectTopRightCoords;
				//coords.V2 = rectBottomRightCoords;
				//coords.V3 = rectTopLeftCoords;
				
				// ODD col num
				//coords.V1 = rectBottomLeftCoords;
				//coords.V2 = rectTopLeftCoords;
				//coords.V3 = rectBottomRightCoords;



				path = new SKPath();
				path.MoveTo(coords.V1);
				path.LineTo(coords.V2);
				path.LineTo(coords.V3);
				path.LineTo(coords.V1);

				// NOTE: path's origin is top left and x and y axes are swapped ( ie 90% rotation ) - not clear why!

				path.Close();				


			} catch (Exception ex) {
				// TODO: Endpoint exception reporting.
				throw;

			}

			return path;

		}

		/// <summary>
		/// Workout path coordinates (V1x,V1y), (V2x, V2y), (V3x, V3y) for given row and col numbers.
		/// </summary>
		/// <param name="info"></param>
		/// <param name="row"></param>
		/// <param name="col"></param>
		/// <returns></returns>
		private TriangleCoords GetTriangleCoords(SKImageInfo info, int row, int col, out SKRectI cellRect)
		{
			TriangleCoords coords;


			// We need the clip coords for the NEXT or PRECEDING triangle col so that we can mask it out ( black ) in the rectangular image
			// that will then the show the row-col triangle.
			if (col % 2 == 0)
			{
				// EVEN col, mask previous.
				col--;
			}
			else
			{
				// ODD column, mask next.
				col++;
			}
			
			var colWidth = (info.Width / (ColCount / 2)); // triangular columns overlap to share width.
			var rowHeight = (info.Height) / RowCount;

			// invert row assignment number ( origin on logical grid is top left, in SKBitmap is bottom left )
			// row = RowCount - row + 1;

			// Init SKRectI for the two triangle columns in which target triangle resides.
			// Note rows will be reversed - TODO: flip.
			int leftPos = (col - 1) / 2 * colWidth;
			int rightPos = (col / 2) * colWidth;

			// TODO: Fix original rect top/bottom inversion ( confusion with top lef origin ).
			cellRect = new SKRectI( leftPos,
									(row * rowHeight), // NOTE: Top should be LOWER than bottom for top lef origin but no time to fix!
									((col % 2 > 0) ? (rightPos + 10) : (rightPos)), // ODD cols need +10 for right coord.
									((row-1) * rowHeight));

			// NOTE: SKRectI origin is top left so height is a negative value, although confusingly Y scale is inverted to be positive below the X axis!!
			// I may have misunderstood the outcome is as described
			// Ref: https://skia.org/user/api/SkRect_Reference#SkRect_height
			SKPointI rectBottomLeftCoords  = new SKPointI(cellRect.Left, cellRect.Top ); // Bottom left Y = TopLeft Y + 10
			SKPointI rectTopLeftCoords     = new SKPointI(cellRect.Left, cellRect.Top + cellRect.Height);
			SKPointI rectTopRightCoords    = new SKPointI(cellRect.Left + cellRect.Width, cellRect.Top + cellRect.Height);  
			SKPointI rectBottomRightCoords = new SKPointI(cellRect.Left + cellRect.Width, cellRect.Top ); // Bottom right Y = TopRight Y + 10

			// Path coords to mask out the other triangle in the cell.
			// NOTE: path's origin is top left and x and y axes are swapped ( ie 90% rotation ) - not clear why!
			if (col % 2 == 0)
			{
				// V1 = opposite hypotenuse
				// even col = top right of cellRect
				coords.V1 = rectTopLeftCoords;

				// V2 next vertex clockwise
				coords.V2 = rectTopRightCoords;

				// V3 next vertex clockwise
				coords.V3 = rectBottomRightCoords;

			}
			else
			{
				// V1 = opposite hypotenuse
				// odd col = bottom left of cellRect
				coords.V1 = rectBottomRightCoords;

				// V2 next vertex clockwise
				coords.V2 = rectBottomLeftCoords;

				// V3 next vertex clockwise
				coords.V3 = rectTopLeftCoords;
				
			}

			
			return coords;

		}

		private int GetRowNumberForLetters(string row)
		{
			return row.ToUpper()[0] - 64; // ASCII char code offset
		}




		#endregion


	}

	public struct TriangleCoords
	{
		public SKPointI V1, V2, V3;

		public TriangleCoords(SKPointI v1, SKPointI v2, SKPointI v3)
		{
			V1 = v1;
			V2 = v2;
			V3 = v3;
		}

	}
}