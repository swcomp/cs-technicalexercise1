﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using ImageGridSamplerWebApi.Services;
using SkiaSharp;

namespace ImageGridSamplerWebApi.Models
{
	public class ImageGrid
	{
		protected ImageSampler Sampler;

		public ImageGrid(ImageSampler imageSampler)
		{
			Sampler = imageSampler;

		}

		public IImageGridClipFilePaths ImageClipFilePaths { get; set;}

		public IEnumerable<IImageGridClipFilePaths> ImageClipFilePathsCollection { get; set; }

		public List<ImageGridRow> Rows { get; private set; }
		public List<ImageGridColumn> Cols { get; private set; }

		public virtual int RowCount { get; set; }

		public virtual int ColCount { get; set; }

		public virtual SKPath GetClipPath(string rowNum, string colNum, out SKRectI cellRect)
		{
			throw new NotImplementedException();
		}
	}
}