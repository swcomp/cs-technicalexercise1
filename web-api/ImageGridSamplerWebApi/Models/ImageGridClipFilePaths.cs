﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImageGridSamplerWebApi.Models
{
	public class ImageGridClipFilePaths : IImageGridClipFilePaths
	{
		public string ClippedImagePath { get; set; }

		public string ClippedImageCellHighlightPath { get; set; }
	}
}