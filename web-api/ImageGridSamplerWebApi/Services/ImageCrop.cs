﻿using ImageGridSamplerWebApi.Models;
using SkiaSharp;
using System;
using System.IO;

namespace ImageGridSamplerWebApi.Services
{
	public class ImageCrop : ImageSampler, IImageCrop
	{

		private IImageGridClipFilePaths _imageGridClipFilePaths;

		public ImageCrop(IImageGridClipFilePaths imageGridClipFilePaths)
		{
			_imageGridClipFilePaths = imageGridClipFilePaths;
		}
		
		private string _triangleCellHighlightImagePath;
		private string _triangleImagePath;

		public SKBitmap ProcessedBitmap { get; private set; }

		public ImageGrid Grid {  get; private set;}

		public void LoadBitmap(string sourceImagePath)
		{
			LoadSkiaBitmap(sourceImagePath);
			
		}

		public IImageGridClipFilePaths DoTriangleCrop(string sourceImagePath, string row, string column)
		{
			SKBitmap triangleClip;

			//_triangleImagePath = AssemblyDirectory + @"\Content\Images\Processed\TriangleCrops\" + row + column + "triangle.jpg";
			string p = AppDomain.CurrentDomain.BaseDirectory;
			// TODO: Fix paths for unit test running.
			_triangleImagePath =  @"..\..\..\..\..\ImageGridSamplerWebApi\bin\Content\Images\Processed\TriangleCrops\" + row + column + "triangle";
			_triangleCellHighlightImagePath = _triangleImagePath + "CellHighlight.jpg";
			_triangleImagePath += ".jpg";


			LoadBitmap(sourceImagePath);

			//bool test = SaveImageClipToFile(SourceBitmap, _triangleImagePath);

			// TODO: fix unit tests so the default grids and rows can be read from Web.config instead of hard coded here ( see TriangleImageGrid(ImageSampler) constructor notes... ).
			//Grid = new TriangleImageGrid(this);
			Grid = new TriangleImageGrid(this,"6","12");

			SKCanvas canvas = new SKCanvas(SourceBitmap);


			// -----------------------------------

			SKImageInfo info = SourceBitmap.Info;

			SKRectI cellRect;
			SKPath clipPath = Grid.GetClipPath(row, column, out cellRect);

			SKRegion clipRegion = new SKRegion();
			bool nonEmpty = clipRegion.SetRect(cellRect);

			nonEmpty = clipRegion.SetPath(clipPath);



			// draw a rectangle with a red border
			var paint = new SKPaint
			{
				Style = SKPaintStyle.Stroke,
				Color = SKColors.Red,
				StrokeWidth = 1
			};
			canvas.DrawRect(cellRect, paint);


			//canvas.Clear();

			SKRect bounds;
			//clipPath.GetTightBounds(out bounds);

			canvas.ClipPath(clipPath);

			canvas.ResetMatrix();
			canvas.DrawPath(clipPath, new SKPaint());

			// Apply triangle clip to Bitmap
			canvas.DrawRegion(clipRegion, new SKPaint());
			//canvas.DrawPath(clipPath, new SKPaint());

			using (var image = SKImage.FromBitmap(SourceBitmap))
			{
				//var subset = image.Subset(cellRect); // bounding rect for triangle clip.

				// TODO: apply triangle clip to subset here.



				// NOTE currently not applying clip as debugging rectangle subset 'clip'
				// TODO: pass triangle clipped subset.
				SaveImageClipToFile(image, _triangleCellHighlightImagePath, cellRect);
			
				// Get a subset of the image with just the triangle bounding box.
				// TODO: Fix original rect top/bottom inversion ( confusion with top lef origin ).
				// Get new cellRect with top and bottom values flipped ( due to error in building original rect - no time to fix )
				SKRectI adjRect = new SKRectI(cellRect.Left, cellRect.Bottom, cellRect.Right, cellRect.Top);
				var subset = image.Subset(adjRect);
				SaveImageClipToFile(subset, _triangleImagePath, cellRect);
			}

			_imageGridClipFilePaths.ClippedImageCellHighlightPath = _triangleCellHighlightImagePath;
			_imageGridClipFilePaths.ClippedImagePath = _triangleImagePath;

			return _imageGridClipFilePaths;
		}

		private void SaveImageClipToFile(SKImage image, string filePath, SKRectI cellRect)
		{
			using (var data = image.Encode(SKEncodedImageFormat.Jpeg, 90))
			{
				// save the data to a stream
				using (var stream = File.OpenWrite(filePath))
				{
					data.SaveTo(stream);
					stream.Close();
					stream.Dispose();
				}
			}

		}


		


	}
}