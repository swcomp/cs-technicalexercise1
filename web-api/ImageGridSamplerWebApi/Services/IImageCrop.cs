﻿using ImageGridSamplerWebApi.Models;
using SkiaSharp;

namespace ImageGridSamplerWebApi.Services
{
	public interface IImageCrop
	{
		IImageGridClipFilePaths DoTriangleCrop(string sourceImagePath, string row, string column);
	}
}