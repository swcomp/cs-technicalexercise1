﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using SkiaSharp;

namespace ImageGridSamplerWebApi.Services
{
	public class ImageSampler
	{
		
		public SKBitmap SourceBitmap { get; private set;}

		/// <summary>
		/// Get SkiaBitmap for image file path.
		/// </summary>
		/// <param name="imagePath"></param>
		/// <returns></returns>
		/// <remarks>
		/// Ref: http://visualcsharpdev.blogspot.com/2017/04/net-core-image-processing.html
		/// https://docs.microsoft.com/en-us/dotnet/api/skiasharp.skmanagedstream?view=skiasharp-1.60.3
		/// </remarks>
		public void LoadSkiaBitmap(string imagePath)
		{
			using (var input = File.OpenRead(imagePath))
			{
				using (var inputStream = new SKManagedStream(input))
				{
					SourceBitmap = SKBitmap.Decode(inputStream);
				}
			}
		}

		/// <summary>
		/// Gets directory of assembly code is in.
		/// </summary>
		/// <remarks>
		/// Ref: https://stackoverflow.com/a/283917
		/// TODO: Refactor code snippet also used in ImageGridSamplerWebApiSpecs
		/// updated for dll dir: https://stackoverflow.com/a/52956
		/// </remarks>
		public static string AssemblyDirectory {
			get {
				//string codeBase = Assembly.GetExecutingAssembly().CodeBase;
				//UriBuilder uri = new UriBuilder(codeBase);
				//string path = Uri.UnescapeDataString(uri.Path);
				string path = System.Reflection.Assembly.GetAssembly(typeof(ImageSampler)).Location;
				return Path.GetDirectoryName(path);
			}
		}
	}
}