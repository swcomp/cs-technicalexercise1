# cs-technicalexercise1

A technical excerise to demonstrate software engineering skills.

__IMPORTANT:__
__This project is in a pre-alpha state:__

_Currently the image cropping service can only be used through the unit tests written in:
"cs-technicalexercise1\web-api\ImageGridSamplerWebApiSpecs\Services\describe_ImageCrop.cs", see "Development Notes" further below._

**NOTE: Confirmed solution builds and unit tests work from GitHub repo clean checkout .**


## The Brief
_( reworded without imagery )_

1A: Build a C# Web API that can subdivide a 60 pixel square image into a right angled triangluar grid with each 10 pixel non-hypotenuse side of each triangle being aligned to either a row(A-F) or column(1-12) and then produce any of the triangles by their grid coordinates.

1B: Given the vertex coordinates (V1x, V1y), ( V2x, V2y), (V3x, V3y) calculate the row and column for the triangle.

## My Objectives
Demonstrate skills in the following areas:

* Full dev lifecycle process
* Business requirements analysis
* Strategic dev stack selection
  * stability
  * performance
  * future proofing
* Software modelling & design
* BDD
* Unit Testing
  * NSpec ( C# .NET )
  * Jasmine ( Javascript )
* E2E Testing
  * Protractor & Selenium
* Integration Testing ( if uploaded images are saved )
  * Protractor
* C# Web API
* Angular javascript SPA client
* QA and Testing
* DevOps
  * UAT Deployment

## Repository Structure

### Folders

#### docs
Requirements and design documentation.

#### web-api
C# Web Api for image processing service.

#### web-client
Javscript Angular SPA client


## Development Notes

### NSpec

The main specs are in the __describe_ImageCrop.cs__ file:

"cs-technicalexercise1\web-api\ImageGridSamplerWebApiSpecs\Services\describe_ImageCrop.cs"

_Occasionally the solution does not build but always succeeds on the next attempt - I've not had time to investigate yet._


__IMPORTANT:__ Currently the filestream operations do not overwrite the produced images... these files are automatically deleted from the following folder before each test run - 
however as a precaution the this folder should be monitored during test runs:

cs-technicalexerise1\web-api\ImageGridSamplerWebApi\bin\Content\Images\Processed\TriangleCrops


NSpecRunner will be located here once NuGet packages are restored: cs-technicalexerise1\web-api\packages\NSpec.3.1.0\tools\net451\win7-x64
Open command prompt and cd to the above directory then execute the following command to run the NSpec tests:

NSpecRunner "..\\..\\..\\..\\..\\ImageGridSamplerWebApiSpecs\bin\Debug\ImageGridSamplerWebApiSpecs.dll"

"..\..\..\..\..\ImageGridSamplerWebApiSpecs\bin\Debug\ImageGridSamplerWebApiSpecs.dll"


Full suite of tests produces 144 files required. We're producing two image files for each image clip: 
The small triangle clip = [Row][Col]triangle.jpg
The full size image with triangle cell selected = [Row][Col]triangleCellHighlight.jpg

A future iteration will support PNGs so the black area ( mask for triangle not seen in cell square ) could be made transparent.

The reference files for the NSpec unit tests are located here:
cs-technicalexerise1\web-api\ImageGridSamplerWebApiSpecs\Images\Expected each test will verify results against these files with image byte array comparisons.


#### Image File Outputs

The following files are produced by the NSpec tests:

FOLDER:
cs-technicalexerise1\web-api\ImageGridSamplerWebApi\bin\Content\Images\Processed\TriangleCrops

The cropped triangle for every cell: [row][cell]triangle.jpg							( would be returned to client)
Square Cell highlight for every triangle: [row][cell]triangleCellHighlight.jpg	( confirms correct location clipped )



### Docker

The WebAPI has been developed with Docker support so on opening solution you will encounter Docker errors if Docker is not running and set to use Windows containers, this can be ignored and builds will work correctly.

_NOTE: At this point the Docker image has not been tested._
